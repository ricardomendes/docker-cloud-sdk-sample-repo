# How to Contribute

We'd love to accept your patches and contributions to this project. There are
just a few small guidelines you need to follow.

## Code reviews

All submissions, including submissions by project members, require review. We
use GitLab merge requests for this purpose. Consult
[GitLab Help](https://docs.gitlab.com/ee/user/project/merge_requests/) for more
information on using merge requests.
