FROM docker:19.03.12

# --------------------------------------------------------------------------
# Install and configure gcloud.
# https://github.com/GoogleCloudPlatform/cloud-sdk-docker/blob/a1754f1ccfa47c4cc5bd218a592d7df1051aaad6/alpine/Dockerfile
# for reference.
# --------------------------------------------------------------------------
ARG CLOUD_SDK_VERSION=361.0.0
ENV CLOUD_SDK_VERSION=$CLOUD_SDK_VERSION
ENV PATH /google-cloud-sdk/bin:$PATH
RUN addgroup -g 1000 -S cloudsdk && \
    adduser -u 1000 -S cloudsdk -G cloudsdk
RUN apk --no-cache add \
        bash \
        curl \
        git \
        gnupg \
        libc6-compat \
        openssh-client \
        py3-crcmod \
        py3-openssl \
    && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    tar xzf google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    rm google-cloud-sdk-${CLOUD_SDK_VERSION}-linux-x86_64.tar.gz && \
    gcloud config set core/disable_usage_reporting true && \
    gcloud config set component_manager/disable_update_check true && \
    gcloud config set metrics/environment github_docker_image && \
    gcloud components install beta && \
    gcloud --version
RUN git config --system credential.'https://source.developers.google.com'.helper gcloud.sh
VOLUME ["/root/.config"]
